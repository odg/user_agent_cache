#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0
"""
Get user agents strings from ua_spoofer, and save in several formats, for use
as a backup cache.
"""
import json
from datetime import datetime, timezone
from pathlib import Path

from ua_spoofer import UserAgent

now = datetime.now(timezone.utc)
timestamp = int(now.timestamp())
ua = UserAgent()
ua_list = "\n".join(ua.all)

browsers = {k: sorted(v) for k, v in ua.dict.items()}
ua_json = {
    "browsers": browsers,
    "last_updated_utc": timestamp,
}
ua_json = json.dumps(ua_json, indent=1)

ua_md = "# Common User Agent Strings\n\nLast updated: {}\n".format(
    now.strftime("%Y-%m-%d %H:%M %Z")
)
for browser, strings in browsers.items():
    ua_md += "\n## {}\n\n".format(browser.replace("-", " ").title())
    for uas in strings:
        ua_md += " * {}\n".format(uas)

output = Path("cache")
output.mkdir(exist_ok=True)
output /= "user_agents.json"
output.write_text(ua_json)
output.with_suffix(".txt").write_text(ua_list)
output.with_suffix(".md").write_text(ua_md)
